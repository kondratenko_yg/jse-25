package ru.kondratenko.nlmk;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.logging.Logger;

public class ListCSV {

    public static final Logger logger = Logger.getLogger(ListCSV.class.getName());

    public static final String CSV_SEPARATOR = ";";

    public static final String FILE_NAME = "list.csv";

    public void writeToCSV(List<Object> objects) throws IllegalAccessException, IOException, IllegalArgumentException {
        Class<?> clazz1 = checkList(objects);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(FILE_NAME), StandardCharsets.UTF_8));
        bw.write(makeTitle(clazz1));
        bw.newLine();
        for (Object object : objects) {
            StringBuilder oneLine = new StringBuilder();
            Class<?> clazz = object.getClass();
            for (Field field : clazz.getDeclaredFields()) {
                field.setAccessible(true);
                String objectFromField = field.get(object) == null ? "" : field.get(object).toString() ;
                if(objectFromField.contains(CSV_SEPARATOR)){
                    objectFromField = "WRONG FIELD!!!";
                }
                oneLine.append(objectFromField);
                oneLine.append(CSV_SEPARATOR);
            }
            bw.write(removeLastSymbol(oneLine.toString()));
            bw.newLine();
        }
        bw.flush();
        bw.close();
    }

    private static String makeTitle(Class<?> clazz1) {
        StringBuilder oneLineStart = new StringBuilder();
        for (Field field : clazz1.getDeclaredFields()) {
            oneLineStart.append(field.getName().trim().length() == 0 ? "" : field.getName());
            oneLineStart.append(CSV_SEPARATOR);
        }
        return removeLastSymbol(oneLineStart.toString());
    }

    private static String removeLastSymbol(String str) {
        return str.substring(0, str.length() - 1);
    }

    private static Class<?> checkList(List<Object> objects) throws IllegalArgumentException {
        if (objects == null || objects.isEmpty()) throw new IllegalArgumentException();
        Class<?> clazz1 = objects.get(0).getClass();
        for (Object object : objects) {
            if (clazz1 != object.getClass()) {
                throw new IllegalArgumentException();
            }
        }
        return clazz1;
    }

}
