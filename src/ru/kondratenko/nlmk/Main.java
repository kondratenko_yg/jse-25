package ru.kondratenko.nlmk;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static ru.kondratenko.nlmk.ListCSV.CSV_SEPARATOR;
import static ru.kondratenko.nlmk.ListCSV.FILE_NAME;

public class Main {
    public static final Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        ListCSV listCSV = new ListCSV();
        List<Object> list = new ArrayList<>();
        //list.add(1);
        list.add(new Person("Ivan", "Ivanov"));
        list.add(new Person("Roman", "Ivanov", "ljhg@mail.ru"));
        list.add(new Person("Roman", "Ivanov", LocalDate.of(1914, 12, 31), "ljhg@mail.ru"));
        try {
            listCSV.writeToCSV(list);
        } catch (IllegalAccessException | IllegalArgumentException | IOException e) {
            logger.severe(e.toString()); return;
        }
        logger.info("File " + FILE_NAME + " is overwritten with separator \""+CSV_SEPARATOR+"\".");
    }


}
